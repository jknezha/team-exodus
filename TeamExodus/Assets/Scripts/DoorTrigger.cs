﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class DoorTrigger : MonoBehaviour {

	public int nextSceneIndex;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("PlayerTrigger"))
		{
			SceneManager.LoadScene (nextSceneIndex);
        }
    }
}
